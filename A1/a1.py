import cv2
import time
import numpy as np


# Part 4 & 5
def locate_rect(frame, red_filter = 0):
    if isinstance(red_filter,np.ndarray):
        max_x = cv2.minMaxLoc(frame[:,:,1],red_filter)[3][0]
        max_y = cv2.minMaxLoc(frame[:,:,1],red_filter)[3][1]
    else:
        max_x = cv2.minMaxLoc(frame)[3][0]
        max_y = cv2.minMaxLoc(frame)[3][1]

    return max_x,max_y


def manual_brightness(gray):
    # if red:
    max_val = 0
    x,y = gray.shape
    for i in range(x):
        for j in range(y):
            if (gray[i][j] >= max_val):
                coord1 = j
                coord2 = i
                max_val = gray[i][j]
    return coord1, coord2
    

def manual_redness(hsv,lower_bounds):
    max_val1 = lower_bounds[1]
    max_val2 = lower_bounds[2]
    x,y,_ = hsv.shape

    coord1 = 0
    coord2 = 0
    for i in range(x):
        for j in range(y):
            if (hsv[i,j][0]==0 and hsv[i,j][1] >= max_val1 and hsv[i,j][2] >= max_val2):
                coord1 = j
                coord2 = i

    return coord1, coord2




def main():
    
    cap = cv2.VideoCapture(0) # For the webcam
    # cap = cv2.VideoCapture(1) # For the phone https://www.e2esoft.com/ivcam/
    # The time of the previous frame
    prev_time = 0   

    # The time of the new frame
    curr_time = 0
    while(True):
        ret, frame = cap.read()
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

        curr_time = time.time()

        # Calculating number of frame processed in given time frame
        fps = 1 / (curr_time - prev_time)
        prev_time = curr_time
        # Cleaning fps value to int and str to display
        fps = int(fps)
        fps = str(fps)

        # Displaying on the frame
        cv2.putText(frame, fps, (7, 70), cv2.FONT_HERSHEY_SIMPLEX, 3, (0, 0, 255), 3, cv2.LINE_AA)

        # # Defining the red hue
        lower = np.array([0,100,100])
        upper = np.array([0,255,255])
        red_filter = cv2.inRange(hsv, lower, upper)


        # # Part 4 & 5
        #  Locating the brightest spot
        coord1, coord2 = locate_rect(gray)
        #  Locating the reddest spot
        coord3, coord4 = locate_rect(hsv,red_filter)

        # print(frame)

        # Part 6

        # coord1,coord2 = manual_brightness(gray)
        # coord3, coord4 = manual_redness(hsv,lower)


        frame = cv2.circle(frame, (coord1, coord2), 10, (255, 0, 0), 2)
        frame = cv2.circle(frame, (coord3, coord4), 10, (0, 0, 255), 2)



        cv2.imshow('frame',frame)
        # print(fps)
        
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    cap.release()
    cv2.destroyAllWindows()

main()
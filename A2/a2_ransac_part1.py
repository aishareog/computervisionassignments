
import numpy as np
import cv2 
import time
from matplotlib import pyplot as plt
# img = cv2.imread('Assignments/sampleLine.jpg',0)
cap = cv2.VideoCapture(0)
prev_time = time.time()
# cap = cv2.VideoCapture(1) # IVcam
while(True):
    ret, frame = cap.read()

    img = frame[100:480, 0:640]
    
    edges = cv2.Canny(img,100,200)

    points = np.argwhere(edges != 0)


    iterations = 0
    inliers = 0
    max_inliers = 0 


    k = 10
    # t = 0.05  
    d = 0.5 # Minimum distance to classify as inlier
    edge1 = np.array([0,0])
    edge2 = np.array([0,0])
    # fitted_inliers = []
    
    fitted_inliers_x = []
    fitted_inliers_y = []
     
    while iterations <= k:
        
        inliers_points_x = []
        inliers_points_y = []
        inliers = 0
        # Acqiuring 2 points
        if(len(points)==0): # If nothing is detected
            p1 = np.array([0,0])
            p2 = np.array([0,0])
            inliers_points_x.append(p1[0])
            inliers_points_x.append(p2[0])
            inliers_points_y.append(p1[1])
            inliers_points_y.append(p2[1])
        else:               # Selecting 2 random values in detected edge
            p1 = points[np.random.randint(0,len(points))]
            p2 = points[np.random.randint(0,len(points))]
            inliers_points_x.append(p1[0])
            inliers_points_x.append(p2[0])
            inliers_points_y.append(p1[1])
            inliers_points_y.append(p2[1])

        for point in points:
        
            # Calculating distance from each point
            # https://stackoverflow.com/questions/39840030/distance-between-point-and-a-line-from-two-points
            dist = np.linalg.norm(np.cross(p2-p1, p1-point))/np.linalg.norm(p2-p1)
            # print(dist)
            if (dist < 2):
                # print('made it here')
                inliers_points_x.append(point[0])
                inliers_points_y.append(point[1])
                inliers += 1

        if inliers > max_inliers:
            max_inliers = inliers
            edge1 = p1 
            edge2 = p2
            fitted_inliers_x = inliers_points_x
            fitted_inliers_y = inliers_points_y
        # print(inliers)
        
        
        iterations += 1

    img = cv2.line(img,(edge1[1],edge1[0]),(edge2[1],edge2[0]),(255,0,0), 2)

    edges = cv2.line(edges,(edge1[1],edge1[0]),(edge2[1],edge2[0]),(255,0,0), 2)

    curr_time = time.time()

    # Calculating number of frame processed in given time frame
    fps = 1 / (curr_time - prev_time)
    # print(curr_time - prev_time)
    prev_time = curr_time
    # Cleaning fps value to int and str to display
    fps = int(fps)
    fps = str(fps)

    # Displaying on the frame
    cv2.putText(img, fps, (7, 70), cv2.FONT_HERSHEY_SIMPLEX, 3, (0, 0, 255), 3, cv2.LINE_AA)
    

        

    cv2.imshow('frame',img)
    cv2.imshow('edge',edges)
    # cv2.imshow("Detected Lines (in red) - Standard Hough Line Transform", cdst)
    # cv2.imshow("Detected Lines (in red) - Probabilistic Line Transform", cdstP)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()



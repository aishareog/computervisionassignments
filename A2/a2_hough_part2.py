import numpy as np
import cv2 as cv
import time
import math

def l_inter(line1, line2):
    r1, t1 = line1;r2,t2 = line2
    A= np.array([[np.cos(t1),np.sin(t1)],[np.cos(t2),np.sin(t2)]])
    b= np.array([[r1],[r2]]);x0,y0=(0,0)
    if(abs(t1-t2)>0):
        return [[np.round(np.linalg.solve(A, b))]]
    # Todo: else statement?
def points_inter(lines):
    intersections = []
    for i, g in enumerate(lines[:-1]):
        for g2 in lines[i+1:]:
            for line1 in g:
                for line2 in g2:
                    if(l_inter(line1, line2)):
                        intersections.append(l_inter(line1, line2)) 

    # print('intersections')
    # print(intersections)
    return intersections


# C:\Users\aisha\Videos\Captures
def clean_image(img):
    med = cv.medianBlur(img, 5)
    gray=cv.cvtColor(med,cv.COLOR_BGR2GRAY)
    
    kernel = np.ones((2,2),np.uint8)
    dilation = cv.dilate(gray,kernel,iterations =3)
    # blur =cv.GaussianBlur(dilation,(3,3),0)
    # blur= cv.erode(blur,kernel,iterations =5)
    edge=cv.Canny(dilation,100,200)
    return gray,dilation,edge


def plot_lines(frame,lines):
    
# print(len(lines))
    if lines is not None:
        for i in range(0, len(lines)):
            rho = lines[i][0][0]
            theta = lines[i][0][1]
            a = math.cos(theta)
            b = math.sin(theta)
            x0 = a * rho
            y0 = b * rho
            pt1 = (int(x0 + 1000*(-b)), int(y0 + 1000*(a)))
            pt2 = (int(x0 - 1000*(-b)), int(y0 - 1000*(a)))
            # x,y = intersection(pt1[0],pt1[1],pt2[0],pt2[1])

            cv.line(frame, pt1, pt2, (0,255,0), 3, cv.LINE_AA)

def plot_intersections(frame,intersection_points):
    for i in range(len(intersection_points)):
        point = intersection_points[i]
        # print(int(point[0]))

        cv.circle(frame, (int(point[0]), int(point[1])), 3, (0, 0, 255), 3)
    return frame



def get_inter_points(edge,org):
    t=300;j=0
    # linesP = [0]
    # print(len(linesP))
    while(j<=4 and t>0):     
        try:linesP=cv.HoughLines(edge,1,np.pi/180,t);j=linesP.shape[0]
        except:j=0
        t=t-10

    # print('This should print twice outside')
    t=0;c=0;lu=[]
    if linesP is not None:
        # print('This should print twice')
        plot_lines(org,linesP)

    
    # print(len(linesP))
    # if j >= 8:
        lines=linesP.reshape(linesP.shape[0],2)
    # print('linesp')
    # print(linesP)

        for l in lines:
            c=c+1;rho,theta=l
            for lt in lines[c:]:
                t=0
                if(lt[0]!=l[0]):
                    rhot,thetat=lt;k=abs(lt-l)<[50,0.5] 
                    if(k[0] and k[1]):
                        t=-1;break                
            lu.append(l)

    # The 4 most likely lines
    lr=np.asarray(lu[:4]);j=np.reshape(lr,[lr.shape[0],1,2])

    # print(j)

    inter_points = points_inter(j)
    # print('inter points')
    # print(len(inter_points))
    p=np.asarray(points_inter(j)).reshape(len(inter_points),2)
    return p


def clean_intersections(points,frame):
    new_points = []
    # indx = 0
    for point in points:

        if point[0] >=0 and point[1]>=0:
            new_points.append(point)
            # indx +=1

    return np.asarray(new_points)

prev_time = time.time()

# cap = cv.VideoCapture(0)
cap = cv.VideoCapture(1) # IVcam
while(True):
    ret, frame = cap.read()

    img = frame[100:480, 0:640]


    gray,dilation,edge = clean_image(img)

    src_copy = img.copy()
    pts_src = get_inter_points(edge,src_copy)
    pts_src = clean_intersections(pts_src,edge)

    # Read destination image.
    # im_dst=cv.imread("stickyNote3.jpg")
    # im_dst=cv.imread("fig1.jpg")
    # gray1,dilation1,edge1 = clean_image(im_dst)
    # Four corners of the book in destination image.
    # dst_copy = im_dst.copy()
    # pts_dst = get_inter_points(edge1,dst_copy)
    # pts_dst = clean_intersections(pts_dst,edge1)

    # print(pts_src)
    # print(pts_dst)


    # Calculate Homography
    # h, status = cv.findHomography(pts_src, pts_dst)

    # Warp source image to destination based on homography
    # im_out = cv.warpPerspective(im_src, h, (im_dst.shape[1],im_dst.shape[0]))





    if len(pts_src) == 4:
        # # Calculate Homography
        # h, status = cv.findHomography(pts_src, pts_dst)

        # Warp source image to destination based on homography
        # im_out1 = cv.warpPerspective(im_src, h, (im_dst.shape[1],im_dst.shape[0]))

        new_corners =  np.float32([[0,0],[639,0],[0, 479],[639,479]])
        h, _ = cv.findHomography(pts_src, new_corners)
        im_out = cv.warpPerspective(img, h, (640,480))
        # cv.imshow("Warped Source Image", im_out1)
        cv.imshow("out_img",im_out)



    # Display images


    curr_time = time.time()

    # Calculating number of frame processed in given time frame
    fps = 1 / (curr_time - prev_time)
    # print(curr_time - prev_time)
    prev_time = curr_time
    # Cleaning fps value to int and str to display
    fps = int(fps)
    fps = str(fps)

    cv.putText(src_copy, fps, (7, 70), cv.FONT_HERSHEY_SIMPLEX, 3, (0, 0, 255), 3, cv.LINE_AA)
    


    src_copy = plot_intersections(src_copy,pts_src)
    # dst_copy = plot_intersections(dst_copy,pts_dst)
    cv.imshow('edge',edge)
    cv.imshow("Source Image", src_copy)




    if cv.waitKey(1) & 0xFF == ord('q'):
        break


cap.release()
cv.destroyAllWindows()

import cv2 as cv
import numpy as np
import time
from PIL import ImageGrab

# cap = cv.VideoCapture(0)
whT = 320
confThreshold =0.5
nmsThreshold= 0.2

#### LOAD MODEL
## Coco Names
classesFile = "coco.names"
classNames = []
with open(classesFile, 'rt') as f:
    classNames = f.read().rstrip('\n').split('\n')
print(classNames)
## Model Files
modelConfiguration = 'yolov3-tiny.cfg'
modelWeights = 'yolov3-tiny.weights'
# modelConfiguration = "yolov3-320.cfg"
# modelWeights = "yolov3.weights"

net = cv.dnn.readNetFromDarknet(modelConfiguration, modelWeights)
net.setPreferableBackend(cv.dnn.DNN_BACKEND_OPENCV)
net.setPreferableTarget(cv.dnn.DNN_TARGET_CPU)

def findObjects(outputs,img):
    hT, wT, cT = img.shape
    bbox = []
    classIds = []
    confs = []
    for output in outputs:
        for det in output:
            scores = det[5:]
            classId = np.argmax(scores)
            confidence = scores[classId]
            if confidence > confThreshold:
                w,h = int(det[2]*wT) , int(det[3]*hT)
                x,y = int((det[0]*wT)-w/2) , int((det[1]*hT)-h/2)
                bbox.append([x,y,w,h])
                classIds.append(classId)
                confs.append(float(confidence))

    indices = cv.dnn.NMSBoxes(bbox, confs, confThreshold, nmsThreshold)

    for i in indices:
        # i = i[0]
        box = bbox[i]
        x, y, w, h = box[0], box[1], box[2], box[3]
        # print(x,y,w,h)
        cv.rectangle(img, (x, y), (x+w,y+h), (255, 0 , 255), 2)
        cv.putText(img,f'{classNames[classIds[i]].upper()} {int(confs[i]*100)}%',
                  (x, y-10), cv.FONT_HERSHEY_SIMPLEX, 0.6, (255, 0, 255), 2)



prev_time = time.time()
while True:
    # start = time.time()
    img = screen = np.array(ImageGrab.grab(bbox=(0,200,800,600)))
    # success, img = cap.read()
    img = img[0:480,0:640]
    blob = cv.dnn.blobFromImage(img,1/255,(whT,whT),[0,0,0,0],1,crop=False)
    #blob as input to the network
    net.setInput(blob)
    #all names of layers
    layerNames = net.getLayerNames()
    #print(layerNames)
    #print(net.getUnconnectedOutLayers())
    outputNames = [layerNames[i-1] for i in net.getUnconnectedOutLayers()]
    # print(outputNames)

    outputs = net.forward(outputNames)
    findObjects(outputs,img)
    # print(outputs.shape)
    # frames_per_sec(start)
    curr_time = time.time()
    fps = 1/(curr_time - prev_time)
    prev_time = curr_time
    cv.putText(img, "FPS: {:.2f}".format(fps), (5, 450),
                cv.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 255), 3)
    cv.imshow("Image",img)
    if cv.waitKey(1) & 0xFF == ord('q'):
        break